drop table if exists reason;
create table reason
(
    r_reason_sk               int,
    r_reason_id               string,
    r_reason_desc             string
)
USING csv
OPTIONS(header "false", path "${TPCDS_GENDATA_DIR}/reason")
;

drop table if exists inventory;
create table inventory
(
    inv_date_sk               int,
    inv_item_sk               int,
    inv_warehouse_sk          int,
    inv_quantity_on_hand      bigint
)
USING csv
OPTIONS(header "false", path "${TPCDS_GENDATA_DIR}/inventory")
;

drop table if exists income_band;
create table income_band
(
    ib_income_band_sk         int,
    ib_lower_bound            int,
    ib_upper_bound            int
)
USING csv
OPTIONS(header "false", path "${TPCDS_GENDATA_DIR}/income_band")
;

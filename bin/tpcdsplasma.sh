#!/bin/bash 

main() {
  bin_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
  source "$bin_dir/tpcdsspark.sh"
  set_env

  #export PLASMA_EXECUTOR_OPTIONS="--conf spark.plasma.sql.gpu.enabled=false"

  if [ "$1" == "--create-tables" ]; then
    create_spark_tables
  elif [ "$1" == "--run-queries" ]; then
    if [ -z "$2" ]; then 
      echo "Missing queries numbers"
      return 1
    fi

    echo "$2" | run_subset_tpcds_queries
  elif [ "$1" == "--run-all-queries" ]; then
    run_tpcds_queries
  elif [ "$1" == "--cleanup" ]; then
    cleanup_all 
  fi
}

[[ "${BASH_SOURCE[0]}" == "${0}" ]] && main "$@"
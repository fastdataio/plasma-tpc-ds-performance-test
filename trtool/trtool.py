#!/usr/bin/env python3

import os
import sys
from testrail import TestRail

MAIN_DIRECTORY = os.path.dirname(
    os.path.abspath(sys.modules["__main__"].__file__)
)

CREDENTIALS = {
    "host": os.getenv("TESTRAIL_HOST", "https://fastdataio.testrail.com"),
    "user": os.getenv("TESTRAIL_USER", "maxim.naydenko@atlasgurus.com"),
    "password": os.getenv("TESTRAIL_PASSWORD", "x7LTyyi5pg0j"),
}


def get_query_index(fullname):
    index = os.path.splitext(os.path.basename(query))[0][len("query") :]
    if index[0] == "0":
        index = index[1:]
    return index


queries_path = os.path.abspath(
    os.path.join(MAIN_DIRECTORY, "../", "src", "queries")
)
queries_files = os.listdir(queries_path)
queries = {}
queries_files.sort()
for query in queries_files:
    with open(os.path.join(queries_path, query), "r") as f:
        # add four spaces to each line for markdown formating in TR
        data = f.read()
        res = ""
        for line in data.splitlines():
            res += "    " + line + "\n"

        name = get_query_index(query)
        queries[name] = res

tr = TestRail(CREDENTIALS["host"], CREDENTIALS["user"], CREDENTIALS["password"])

projects = tr.getProjects()
proj_id = None
for project in projects:
    if project["name"] == "Plasma Laboratory":
        proj_id = project["id"]
        break

if not proj_id:
    print("Failed to find Plasma Laboratory project")
    sys.exit(1)

sections = tr.getSections(proj_id)
section_id = None
for section in sections:
    if section["name"] == "TPC-DS":
        section_id = section["id"]
        break

if not section_id:
    print("Failed to find TPC-DS section")
    sys.exit(1)

templates = tr.getTemplates(proj_id)
template_id = None
for template in templates:
    if template["name"] == "Plasma Test Case":
        template_id = template["id"]
        break

if not template_id:
    print("Failed to find Plasma Case template")
    sys.exit(1)

idx = 1
for name, sql in queries.items():
    print(name)
    tr.addCase(
        section_id,
        {
            "title": name,
            "template_id": template_id,
            "custom_case_source": sql,
            "custom_instance_type": "p3.2xlarge",
            "custom_engine": "plasma",
            "custom_experiment_name": "tpc-ds",
        },
    )

    idx += 1
    if idx == 100:
        break

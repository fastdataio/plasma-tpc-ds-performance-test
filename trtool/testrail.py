import requests
import json
from requests.auth import HTTPBasicAuth


class TestRail(object):
    def __init__(self, addr, user, password):
        self.__addr = addr
        self.__auth = HTTPBasicAuth(user, password)
        self.__headers = {
            "Content-type": "application/json",
            "Accept": "application/json",
        }

    def get(self, route):
        req = self.__addr + "/index.php?/api/v2/" + route
        resp = requests.get(req, auth=self.__auth, headers=self.__headers)
        if resp:
            return json.loads(resp.text)
        else:
            print("`{}` error. GET {}".format(resp.text, req))
        return []

    def post(self, route, data):
        req = self.__addr + "/index.php?/api/v2/" + route
        resp = requests.post(
            req, auth=self.__auth, headers=self.__headers, json=data
        )
        if resp:
            return json.loads(resp.text)
        else:
            print("`{}` error. POST {}".format(resp.text, req))
            return []

    def getTestCasesForRun(self, id):
        return self.get("get_tests/{}".format(id))

    def getTestRunInfo(self, id):
        return self.get("get_run/{}".format(id))

    def getMilestone(self, id):
        return self.get("get_milestone/{}".format(id))

    def setResults(self, test_id, results):
        return self.post("add_result/{}".format(test_id), data=results)

    def getTestPlanInfo(self, id):
        return self.get("get_plan/{}".format(id))

    def getConfigs(self, proj_id):
        return self.get("get_configs/{}".format(proj_id))

    def getSections(self, proj_id):
        return self.get("get_sections/{}".format(proj_id))

    def getProjects(self):
        return self.get("get_projects")

    def getTemplates(self, proj_id):
        return self.get("get_templates/{}".format(proj_id))

    def addCase(self, section_id, data):
        return self.post("add_case/{}".format(section_id), data=data)

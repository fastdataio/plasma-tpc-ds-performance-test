## Running with PlasmaENGINE

Build fdio-hub with Hive support:

```
$ build/mvn -Pyarn -Phive -Phive-thriftserver -DskipTests clean install
```

Set SPARK_HOME to the correct fdio-hub directory in `bin/tpcdsenv.sh`. Run `bin/tpcdsspark.sh`
